# Parallel API testing

A simple script to test an API with either cURL or newman

## POSIX-compliant and (Git) Bash compliant scripts

Most of shells should be able to run the `parallel_api_test_posix.sh` script. However, the program `bc` is not implemented in Git Bash and the likes, so a version with bash arithmetic and awk is used in the `parallel_api_test_bash.sh` version.

## Usage

`parallel_api_test_posix.sh [curl|newman] [URL] ([NEWMAN FOLDER])`

The `NEWMAN FOLDER` refers to the `--folder` argument, not to a directory.

By default, the script performs 1-5, 10, 50 and 100 parallel requests to a given URL and produces a CSV with avg, min and max request times.

The script simply launches either `cURL` or `newman` as multiple background processes. The code should contain comments in the relevant parts.

## Note on newman --folder

The parameter, if it contains spaces, should be given as `'"parameter with spaces"'`

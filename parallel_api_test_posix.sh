#!/bin/sh

# This script is used in testing APIs in parallel using either Newman or cURL
# Usage: <script> [curl|newman] [URL] ([OPTIONS])

# CAPTURE THE COMMAND-LINE PARAMETERS

all_cmdline_params=$#
# The file name of this script
script=$0
# Either "newman" or "curl", i.e. the executable name
program="$1"
# URL to test, e.g., 127.0.0.1:8000
url="$2"

# PRINT HELP IF NEEDED

# This pattern is used in place of if .. else pattern
# && performs the second operation only if the first operation exited with the code 0
# -lt stands for "less than", see 'man test'
# This line is basically "if number of command line arguments is less than 2, print usage text"
[ $all_cmdline_params -lt 2 ] && printf 'Usage: %s [PROGRAM: curl|newman] [URL] [OPTIONS]\n' "$script" && exit

# shift [n] shifts the positional parameters n times.
shift 2
# OPTIONS represent any optional parameters passed to cURL or Newman.
#   * In case of Newman, these could be, for example, --folder TEST_NAME --env-var param=123
#   * In case of cURL, these could be, for example, --data '{"x": {"y": "z"}}'
# "$*" captures the rest of the command-line parameters as string.
options="$*"

# CONSTANTS

# By default, the script tests 1-5, 10, 50 and 100 requests in parallel
N_TESTS="$(seq 5) 10 50 100"
RESULT_DIR="requests-results"
RESULT_CSV="$(date '+%Y-%m-%d_')results.csv"
NEWMAN_TESTS_FILE="../postman_tests.json"
NEWMAN_PARSER="python -c \"import sys,json; print(json.load(sys.stdin)['run']['executions'][0]['response']['responseTime'])\""

# We are only interested in the total time making the request, thus the -w option
CURL_COMMAND_BASE="curl -w %%{time_total}, -o /dev/null -s \"$url\""

NEWMAN_COMMAND_BASE="newman run --env-var url=${url} ${NEWMAN_TESTS_FILE} --reporters json"

# The -z tests if a given string is an empty string, thus not given
[ -z "$options" ] || NEWMAN_COMMAND_BASE="${NEWMAN_COMMAND_BASE} ${options}"

mkdir -p $RESULT_DIR
cd $RESULT_DIR || exit 1
printf "n_parallel,avg,min,max\n">$RESULT_CSV

newman_run() { \
	command=$(printf "${NEWMAN_COMMAND_BASE} --reporter-json-export %d.tmp & \n" $(seq 1 100))
	for n_test in $(echo "$N_TESTS"|tr " " "\n")
	do
		printf 'Running with %s parallel requests.\n' "$n_test"
		run_command=$(echo "$command"|head -n "$n_test")
		# As this is run in subshell the command must have its own "wait"
		echo "$run_command wait"|sh
		wait
		for result_file in $(find . -type f -name "*.tmp")
		do
		cat $result_file|sh -c "$NEWMAN_PARSER" >>"${n_test}_results.txt"
		done
		rm ./*.tmp
		sum=$(cat "${n_test}_results.txt"|paste -sd+ |bc)
		avg=$(echo "$sum/$n_test"|bc -l)
		min=$(cat "${n_test}_results.txt"|sort -n|head -n1)
		max=$(cat "${n_test}_results.txt"|sort -n -r|head -n1)
		printf "$n_test,$avg,$min,$max\n">>$RESULT_CSV
		rm "${n_test}_results.txt"
	done
}

curl_run() { \
	command=$(printf "${CURL_COMMAND_BASE} >%d.tmp & \n" $(seq 1 100))
	for n_test in $(echo "$N_TESTS"|tr " " "\n")
	do
		printf "Running with $n_test parallel requests.\n"
		run_command=$(echo "$command"|head -n $n_test)
		# As this is run in subshell the command must have its own "wait"
		echo "$run_command wait"|sh
		wait
		cat ./*.tmp|tr ',' '\n' >"${n_test}_results.txt"
		rm ./*.tmp
		sum=$(cat "${n_test}_results.txt"|paste -sd+ |bc)
		avg=$(echo "$sum/$n_test"|bc -l)
		min=$(cat "${n_test}_results.txt"|sort -n|head -n1)
		max=$(cat "${n_test}_results.txt"|sort -n -r|head -n1)
		printf "$(echo "$n_test * 100"|bc),$(echo "$avg * 100"|bc),$(echo "$min * 100"|bc),$(echo "$max * 100"|bc)\n">>$RESULT_CSV
		rm "${n_test}_results.txt"

	done
}
case "$program" in
	# Try to keep these cases in alphabetical order.
	curl) curl_run ;;
	newman) newman_run ;;
	*) printf '%s: Invalid program: %s\n' "$script" "$program" ;;
esac
